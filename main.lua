local anim8 = require 'lib/anim8'
local baton = require 'lib/baton'

local mainCanvasW = 160
local mainCanvasH = 144
local mainCanvas = love.graphics.newCanvas(mainCanvasW, mainCanvasH)

local parallax = require './parallax'
local input = baton.new{
   controls = {
      left = {'key:left', 'key:a', 'axis:leftx-', 'button:dpleft'},
      right = {'key:right', 'key:d', 'axis:leftx+', 'button:dpright'},
      up = {'key:up', 'key:w', 'axis:lefty-', 'button:dpup'},
      down = {'key:down', 'key:s', 'axis:lefty+', 'button:dpdown'},
      action1 = {'key:z', 'button:a', 'key:space'},
      action2 = {'key:x', 'button:b', 'key:escape'},
   },
   pairs = {
      move = {'left', 'right', 'up', 'down'}
   },
   joystick = love.joystick.getJoysticks()[1],
}

local character = {
  anim = "stand-r",
  animations = {},
  moveSpeed = 300
}

function love.load()
   love.window.setMode(mainCanvasW*4, mainCanvasH*4, {
      resizable = true,
      minwidth = mainCanvasW,
      minheight = mainCanvasH,
   })
   love.graphics.setBackgroundColor(0, 0, 0)

   image = love.graphics.newImage('img/guy.png')
   image:setFilter("nearest", "nearest", 0)
   local g = anim8.newGrid(32, 32, image:getWidth(), image:getHeight())

   character.animations["stand-r"] = anim8.newAnimation(g('1-1', 1), 0.1)
   character.animations["walk-r"] = anim8.newAnimation(g('2-4',1, 1,1), 0.1)
   character.animations["stand-l"] = character.animations["stand-r"]
      :clone():flipH()
   character.animations["walk-l"] = character.animations["walk-r"]
      :clone():flipH()

   mainCanvas:setFilter("nearest", "nearest", 0)
   parallax.load()
end

function love.update(dt)
   input:update()
   parallax.update(dt)

   local x, y = input:get 'move'
   parallax.applyMovement(character.moveSpeed * x * dt)

   local startAnim = character.anim
   if x > 0 then
      character.anim = "walk-r"
   elseif x < 0 then
      character.anim = "walk-l"
   elseif character.anim == "walk-r" then
      character.anim = "stand-r"
   elseif character.anim == "walk-l" then
      character.anim = "stand-l"
   end

   if character.anim ~= startAnim then
      character.animations[character.anim]:gotoFrame(1)
   else
      character.animations[character.anim]:update(dt)
   end
end

function love.draw()
   love.graphics.setCanvas(mainCanvas)
   love.graphics.setColor(1, 1, 1)
   parallax.draw()
   character.animations[character.anim]:draw(image, 55, 100, 0)
   love.graphics.setCanvas()

   -- blit mainCanvas to screen with scaling

   local windowW, windowH = love.graphics.getDimensions()
   local mainCanvasScale = math.floor(math.min(
      windowW / mainCanvasW,
      windowH / mainCanvasH
   ))

   love.graphics.clear()
   love.graphics.draw(
      mainCanvas,
      (windowW - mainCanvasW*mainCanvasScale) / 2,
      (windowH - mainCanvasH*mainCanvasScale) / 2,
      0,
      mainCanvasScale,
      mainCanvasScale
   )
end
