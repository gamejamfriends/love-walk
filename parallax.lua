local mainCanvasW = 160
local mainCanvasH = 144
local driftSpeed = 50

local layersDefault = {
   { speed = 0.05, image = love.graphics.newImage("img/parallax1.png") },
   { speed = 0.09, image = love.graphics.newImage("img/parallax1.5.png"), drift = true },
   { speed = 0.19, image = love.graphics.newImage("img/parallax2.png") },
   { speed = 0.29, image = love.graphics.newImage("img/parallax2.5.png"), drift = true },
   { speed = 0.66, image = love.graphics.newImage("img/parallax3.png") }
}

local parallax = {
   layers = layersDefault,
   offset = 0,
   drift = 0
}

function parallax.load()
end

function parallax.update(dt)
   parallax.drift = parallax.drift + dt
end

function parallax.applyMovement(n)
   parallax.offset = parallax.offset + n
end

function parallax.draw()
   love.graphics.setColor(0, 0, 0)
   love.graphics.rectangle("fill", 0, 0, mainCanvasW, mainCanvasH)
   love.graphics.setColor(1, 1, 1)

   for _, layer in pairs(parallax.layers) do
      local imWidth = layer.image:getWidth()

      local totalOffset = parallax.offset
      if layer.drift then
         totalOffset = totalOffset + parallax.drift * driftSpeed
      end
      local x = -math.floor(totalOffset * layer.speed % imWidth)

      while x < mainCanvasW do
         love.graphics.draw(layer.image, x, 0)
         x = x + imWidth
      end
   end
end

return parallax
